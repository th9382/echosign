""" Documentation
# 1. Update api_key and userkey. You need to get the API key from EchoSign, and can then retrieve userkeys via API
# 2. Update sender email
# 3. make sure roster is updated
# 4. make sure files are in directory

# EchoSign SOAP API documentation https://secure.echosign.com/public/docs/EchoSignDocumentService17
"""

import suds
from time import localtime, strftime
import pandas as pd
from pprint import pprint
import os
import sys

ECHOSIGN_API_KEY = ''
ECHOSIGN_USERKEY = ''
CARLOS_EMAIL = 'email@example.com'
HR_EMAIL = 'hr@example.com'
MYPATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), "Comp_plans/")
WSDL_URL = 'https://secure.echosign.com/services/EchoSignDocumentService17?wsdl'

# build subject depending on rep Role
def comp_subject(role):
    subject = "Comp. Plan FY2015 "
    if role == "Role1":
        subject += "Q3-Q4"
    elif role == "ROle2" or role == "ESM":
        subject += "Q3-Q4"
    elif role == "Role3":
        subject += "Q3-Q4"
    elif role == "Role4":
        subject += "Q3-Q4"
    return subject

def roster_dictionary(csv_file):
    """convert csv data to dictionary format"""
    try:
        data = pd.read_csv(csv_file)
    except:
        sys.exit(0)

    roster = {}

    for item in range(len(data.Name)):
        roster[data.Name[item]] = dict([('Role', data.Job_Title[item]), ('Email', data.Email[item]), ('Manager', data.Manager[item]), ('RVP', data.RVP[item]), ('Send', data.Send[item])])
    return roster

def comp_files(dir_path):
    """get excel comp plan files"""
    excel_files = []
    for file_name in os.listdir(dir_path):
        if file_name.endswith(".xlsx"):
            excel_files.append(file_name)
    return excel_files

def error_checking():
    """ Review plans to be sent and check for missing files"""

    roster = roster_dictionary("enterprise_roster.csv")
    pprint(roster)

    excel_files = comp_files(MYPATH)
    
    rep_list = []
    missing_reps = []

    for rep in roster:
        if roster[rep]['Send'] == 'Yes': # only sent to reps where 'Yes' is specified in the Send column in csv file
            if str(rep + " FY2015.xlsx") in excel_files:
                rep_list.append(rep)
            else:
                missing_reps.append(rep)

    if len(missing_reps) > 0:
        print "Not all rep comp plans in folder. Check all files"
        print missing_reps
        raise SystemExit(0)

    print "\n" * 2
    print "All comp excel files are in the folder!"
    return True

def echosign():
    """ Send comp plans with EchoSign API """
    roster = roster_dictionary("enterprise_roster.csv")

    if error_checking() == True:
        # print out list of reps to which comp plans are sent to double check
        for rep in roster:
            if roster[rep]['Send'] == 'Yes':
                print "Rep: %20s \t Manager: %20s \t Rep Email: %30s \t Manager Email: %20s \t RVP: %20s" % (rep, roster[rep]['Manager'],
                    roster[rep]['Email'], roster[roster[rep]['Manager']]['Email'], roster[roster[rep]['Manager']]['RVP'])
        print "\n"

        # Review before proceeding
        go_no_go = raw_input("Does this look correct? Type 'yes' to proceed: ")
        print go_no_go
        if go_no_go != "yes":
            print "Stopped!"
            raise SystemExit(0)

        # Capture outputs
        output = []
        success_send = []

        # Connect to EchoSign API
        client = suds.client.Client(WSDL_URL, autoblend=True)

        for rep in roster:
            if roster[rep]['Send'] == 'Yes':
                rep_name = rep
                manager_name = roster[rep]['Manager']
                rep_email = roster[rep]['Email']
                manager_email = roster[manager_name]['Email']
                comp_sheet_name = rep_name + " " + "FY2015.xlsx"

                ### Build types for EchoSign API sendDocument

                ## Build files
                fileInfoArray = client.factory.create("ns1:ArrayOfFileInfo")

                def files_function(file_name):
                    fileInfo = client.factory.create("ns1:FileInfo")
                    fileInfo.fileName = file_name
                    with open(MYPATH+file_name, "rb") as doc:
                        thefile = doc.read().encode("base64")
                    fileInfo.file = thefile
                    fileInfo.mimeType = None
                    fileInfoArray.FileInfo.append(fileInfo)

                # each plan has a cover sheet and an excel file that is merged and sent via EchoSign
                files_function('Cover_sheet.docx')
                files_function(comp_sheet_name)


                ### Create Recipients
                recipientInfoArray = client.factory.create("ns18:ArrayOfRecipientInfo")

                def recipientsFunction(email_address):
                    recipientInfo = client.factory.create("ns18:RecipientInfo")
                    recipientInfo.email = str(email_address)
                    recipientRole = client.factory.create("ns18:RecipientRole")
                    recipientRole.role = "SIGNER"
                    recipientInfo.role = recipientRole.role
                    recipientInfoArray.RecipientInfo.append(recipientInfo)

                # Build recipient list: 1st Recipient is always Carlos, then Manager, then Rep. 
                recipientsFunction(CARLOS_EMAIL)

                #If Carlos also the Manager, then don't send it twice
                if rep_email == CARLOS_EMAIL or manager_email == CARLOS_EMAIL:
                    pass
                else:
                    recipientsFunction(manager_email)

                recipientsFunction(rep_email)

                ### remaining documentCreationInfo values
                dci = client.factory.create("ns1:DocumentCreationInfo")
                dci.callbackInfo = None

                # Specify cc'ing
                # cc RVPs if RVP specified
                ccs = client.factory.create("ArrayOfString")

                if roster[rep]['RVP'] == "RVP1" or roster[rep]['RVP'] == "RVP2": 
                    ccs.string = [HR_EMAIL, roster[roster[rep]['RVP']]['Email']]   
                else:
                    ccs.string = [HR_EMAIL]

                dci.ccs = ccs

                dci.daysUntilSigningDeadline = None
                dci.externalId = None
                dci.fileInfos = fileInfoArray
                dci.formFieldLayerTemplates = None
                dci.locale = "en_US"
                dci.mergeFieldInfo = None
                dci.mergeFile = None
                dci.message = rep_name[:rep_name.find(" ")] + ", \nPlease review and sign your comp plan. If you have any questions, please talk to your manager. \nThanks!"

                subj = comp_subject(roster[rep]['Role'])

                dci.name = rep_name + " " + subj
                dci.recipients = recipientInfoArray
                dci.reminderFrequency = "DAILY_UNTIL_SIGNED" #set reminder for first signer
                dci.securityOptions = None
                dci.signatureFlow = "SENDER_SIGNATURE_NOT_REQUIRED"
                dci.signatureType = "ESIGN"
                dci.tos = None
                dci.vaultingInfo = None
                sender = client.factory.create("ns1:SenderInfo")
                sender.userKey = ECHOSIGN_USERKEY

                ###### RUN command save output + timestamp to array ######
                output.append([
                    str(client.service.sendDocument(ECHOSIGN_API_KEY, sender, dci)),
                    rep_name, strftime("%a, %d %b %Y %H:%M:%S", localtime())
                    ])

                success_send.append(rep_name)
                print "Sent to %s: " % rep_name
    else:
        print 'Error checking failed'

    #### Save return document IDs + add timestamp ####
    docids = []
    for i in output:
        start = i[0].find('"')
        end = i[0].find('"', start + 1)
        docids.append([i[0][start + 1: end], i[1], i[2]])

    with open("docids.text", "a+") as doc:
        doc.write(str(docids))

    ### print out and save rep names to file
    with open("sent.txt", "a+") as sent:
        sent.write(str(success_send))

if __name__ == "__main__":
    echosign()
